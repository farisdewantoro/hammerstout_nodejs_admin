import db from '../config/conn';
import async from 'async';
import Email from '../../Email';
import moment from 'moment';
import axios from 'axios';
import keys from '../config/keys';
let queryTimeSend = `
select  
	ord.id,ord.user_id,ord.order_status_id,ord.send_notification_pending,
	orb.email,orb.phone_number,orb.firstname,
	ors.status,ors.code,
	orp.payment_type,orp.transaction_time,orp.transaction_status,
	orp.transaction_time + interval 1 day as expired_date,
	orp.transaction_time + interval 6 hour as send_notification_time
from orders as ord
left join order_status as ors on ord.order_status_id = ors.id
left join order_payment as orp on ord.id = orp.order_id
left join order_billing as orb on ord.id = orb.order_id
where ors.id = 3 and (orp.payment_type = 'bank_transfer' or orp.payment_type = 'cstore') and ord.send_notification_pending = 0
order by orp.transaction_time asc limit 1
`;

let queryUpdateNotification = `
update orders as ord set ord.send_notification_pending = ? where ord.id = ? ;
`;



let queryOrderDetail = `
select 
	ord.id,
	orp.payment_type,orp.bank,orp.store,orp.store,
	orp.va_number,orp.payment_code,orp.gross_amount,orp.pdf_url,orp.transaction_time,
	orp.transaction_time + interval 1 day as expired_date
 from orders as ord
 left join order_payment as orp on ord.id = orp.order_id 
 where ord.id = ?
 `;

let queryOrderBilling = `
 select 
	ord.id,
	orb.firstname,
	orb.email,
	orb.phone_number,
	orb.province,
	orb.regency,
	orb.district,
	orb.village,
	orb.postcode,
	orb.address
from orders as ord
left join order_billing as orb on ord.id = orb.order_id
 where ord.id = ?
 `;

let queryOrderItem = `
SELECT 
    p.name as product_name,
    oi.price,
    p.regular_price,
    oi.order_id,
    p.id as product_id,
    pa.size,
    oi.quantity 
    from order_item as oi 
    left join orders as ord on oi.order_id = ord.id
    left join products as p on oi.product_id = p.id
    left join product_attribute as pa on oi.product_attribute_id = pa.id
    where ord.id = ?
 `;



function getOrderDetail(id) {
    return new Promise((res, rej) => {
        db.query(queryOrderDetail, [id], (err, result) => {
            if (err) return rej(err);
            if (result.length > 0) return res(result[0]);
            if (result.length === 0) return rej('NOT FIND');
        })
    })
}

function getOrderBilling(id) {
    return new Promise((res, rej) => {
        db.query(queryOrderBilling, [id], (err, result) => {
            if (err) return rej(err);
            if (result.length > 0) return res(result[0]);
            if (result.length === 0) return rej('NOT FIND');
        })
    })
}
function getOrderItem(id) {
    return new Promise((res, rej) => {
        db.query(queryOrderItem, [id], (err, result) => {
            if (err) return rej(err);
            if (result.length > 0) return res(result);
            if (result.length === 0) return rej('NOT FIND');
        })
    })
}

function handleRejection(p) {
    return p.catch(err => ({ error: err }));
}

async function sendEmail(data){
    let users = [
        {
            email: data.email,

            subject: '[HAMMERSTOUTDENIM] ORDER INFORMATION',
        },

    ];
    let order_billing = await getOrderBilling(data.id);
    let order_detail = await getOrderDetail(data.id);
    let order_item = await getOrderItem(data.id);
    if (order_billing && order_detail && order_item) {
        order_detail.transaction_time = moment(order_detail.transaction_time).format('LLL');
        order_detail.expired_date = moment(order_detail.expired_date).format('LLL');
        users[0].order_detail = order_detail;
        users[0].order_billing = order_billing;
        users[0].order_item = order_item;
    }
    let info = await Email.loadTemplate('welcome', users);
    if(info){
       let sending=  await Email.sendEmail({
                    to: info[0].context.email,
                    from: '"Hammerstoutdenim" <order@hammerstoutdenim.com>',
                    subject: info[0].email.subject,
                    html: info[0].email.html
                });


        if (sending && sending.accepted instanceof Array && sending.accepted.length > 0) {
            return true;
        } else {
            return false;
        }
 
    }
}

async function sendSms(data){
    let message = '';
    let name = data.firstname;
    const order_id = data.id;
    const phone_number = data.phone_number;
    if (name.length > 10) {
        name = name.slice(0, 5) + '..';
    }
    const tanggal_expired = moment(data.expired_date).format('LLL');
    message = `Halo, ${name} \nPembayaran untuk ${order_id} belum kami terima. Tolong lakukan pembayaran sebelum tanggal ${tanggal_expired}. Terimakasih :) HAMMERSTOUTDENIM`;


    let urlSms = `http://45.32.107.195/sms/smsreguler.php?username=${keys.rajasms.username}&key=${keys.rajasms.key}&number=${phone_number}&message=${message}`;

   let sending = await axios.post(urlSms);
    let status = sending.data.match(/^(0)/);
    if (status) {
        return true;
    } else {
        return false;
    }
}



function getTime(){
   return new Promise((res,rej)=>{
        db.query(queryTimeSend,(err,result)=>{
         
            if(err) return rej(err);
            if(result) return res(result);
        })
    });
}

function updateNotification(status,id){
    return new Promise((res, rej) => {
        db.query(queryUpdateNotification, [status,id], (err, result) => {

            if (err) return rej(err);
            if (result) return res(result);
        })
    });
}

const transaction_notification = async (isRunning)=>{
    isRunning = true;
    let currentDate = new Date();
    let time = await getTime();
 
    if(time.length > 0){
        let sendDate = new Date(time[0].send_notification_time);
   
        if (sendDate < currentDate ){
           let sendEmailStatus =  await sendEmail(time[0]);
           let sendSmsStatus = await sendSms(time[0]);
            console.log(`SENDING TO ${time[0].id}`)
            if (sendEmailStatus && sendSmsStatus){
               await updateNotification(1,time[0].id);
            }
            if (sendEmailStatus && !sendSmsStatus){
                await updateNotification(2, time[0].id);
            }
            if (!sendEmailStatus && sendSmsStatus) {
                await updateNotification(3, time[0].id);
            }
            if (!sendEmailStatus && !sendSmsStatus) {
                await updateNotification(4, time[0].id);
            }
      
            isRunning = false;
        }else{
            
            isRunning = false;
        }
    }else{
    
        isRunning = false;
    }
}

export default transaction_notification;