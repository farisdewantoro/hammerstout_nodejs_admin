import midtransClient from 'midtrans-client';
import keys from './keys';
let snap = new midtransClient.Snap({
    isProduction: false,
    serverKey: keys.midtrans.serverKey,
    clientKey: keys.midtrans.clientKey
});


export default snap;