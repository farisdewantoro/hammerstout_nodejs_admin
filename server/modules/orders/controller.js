import db from '../../config/conn';
import async from 'async';
import qs from 'query-string';
import keys from '../../config/keys';
import axios from 'axios';
import snap from '../../config/midtrans';
import XLSX from 'xlsx'
export const getOrderById = (req, res) => {
    if (req.params.id == null || req.params.id == '' || typeof req.params.id === "undefined") {
        let notification = {
            error: true,
            message: "There is an error !",
            notification: true
        }
        return res.status(400).json({ notification: notification });
    }
    let order_id = req.params.id;

    let queryUpdateOrder = `
    UPDATE orders set orders.order_status_id = 2 
        where orders.order_status_id = 1 and  now() > orders.created_at+interval 4 HOUR
    `;

    let queryOrder = `SELECT 
    ord.id,
    ord.user_id,
    ors.status,
    ors.id as order_status_id
    ,ord.created_at 
    from orders as ord 
    left join order_status as ors on ord.order_status_id = ors.id
    where ord.id = '${order_id}' `;

    let queryOrderItems = `SELECT 
    p.name as product_name,
    p.slug as product_slug,
    p.description,
    oi.price,
    oi.order_id,
    c.name as category_name,
    c.slug,
    ct.name as category_type,
    ct.slug as category_type_slug,
    p.id as product_id,
    pa.id as product_attribute_id,
    pv.id as product_variant_id,
    i.link,
    i.caption,
    i.alt,
    pa.size,
    oi.quantity 
    from order_item as oi 
    left join orders as ord on oi.order_id = ord.id
    left join products as p on oi.product_id = p.id
    left join product_category as pc on p.id = pc.product_id 
    left join categories as c on pc.category_id = c.id 
    left join product_attribute as pa on oi.product_attribute_id = pa.id
    left join product_variant as pv on oi.product_variant_id = pv.id
    left join category_type as ct on pv.category_type_id = ct.id
    left join product_image as pi on pi.id = (SELECT pi1.id from product_image as pi1 where pi1.product_id = p.id order by pi1.product_id asc limit 1)
    left join images as i on pi.image_id = i.id
    where ord.id = '${order_id}'`;

    let queryOrderShipment = `SELECT 
    os.courier,
    os.description,
    os.service,
    os.cost,
    os.etd,
    ord.id as order_id 
    from order_shipment as os
    left join orders as ord on os.order_id = ord.id
    where  ord.id = '${order_id}' `;

    let queryOrderVoucher = `SELECT
    v.id as voucher_id,
    v.name as voucher_name,
    v.voucher_type_id as voucher_type,
    v.value,
    ov.order_id
    from order_voucher as ov
    left join vouchers as v on ov.voucher_id = v.id
    left join orders as ord on ov.order_id = ord.id
    where ord.id = '${order_id}'
    `;

    let queryOrderBill = `SELECT * from order_billing where order_id = '${order_id}'`;
    let queryOrderPayment = `SELECT
    op.payment_type,
    op.order_id,
    op.status_code,
    op.transaction_id,
    op.transaction_status,
    op.transaction_time,
    op.pdf_url,
    op.bank,
    op.store,
    op.va_number,
    op.gross_amount,
    op.card_type,
    op.masked_card,
    op.payment_code,
    os.status as order_status_code
    from order_payment as op
    left join orders as ord on op.order_id = ord.id
    left join order_status as os on ord.order_status_id = os.id 
    where ord.id = '${order_id}'
    `;
    let token = `Username:${keys.midtrans.serverKey}:`;

    let queryUpdateStatusOrder = `
    UPDATE orders set orders.order_status_id = (SELECT id from order_status where code = ? limit 1)
    where orders.id = '${order_id}' ;
    UPDATE order_payment set status_code = ? , transaction_status = ? 
    where  order_id = '${order_id}' and transaction_id = ?
    `;
    let queryUpdateOnlyStatus = `UPDATE orders set orders.order_status_id = (SELECT id from order_status where code = ? limit 1)
    where orders.id = ?`;

    let queryUpdatePaymentOrder = ` UPDATE order_payment set status_code = ? , transaction_status = ? 
    where  order_id = '${order_id}' and transaction_id = ?`;
    let queryOrderPaymentInsert = `INSERT into order_payment set ? `;
    // token =  Buffer.from(token).toString('base64');

    async.parallel({
        // UPDATE ORDER IF < 4 HOURS
        updateOrder: function (callback) {
            db.query(queryUpdateOrder, (err, result) => {
                callback(err, "OK");
            })
        },
        order_status: function (callback) {
            //  FIND ORDER PAYMENT
            db.query(queryOrderPayment, (err, result) => {
                if (err) {
                    callback(err, null);
                }
                if (result.length === 0 ||
                    (
                        result.length > 0
                        &&
                        (typeof result[0].payment_status_code === "undefined" || result[0].payment_status_code === '' || result[0].payment_status_code === null)
                        &&
                        (typeof result[0].payment_type === "undefined" || result[0].payment_type === '' || result[0].payment_type === null)
                        &&
                        (typeof result[0].transaction_id === "undefined" || result[0].transaction_id === '' || result[0].transaction_id === null)
                        &&
                        (typeof result[0].transaction_status === "undefined" || result[0].transaction_status === '' || result[0].transaction_status === null)
                    )
                ) {
                    // UPDATE OR INSERT PAYMENT
                    snap.transaction.status(order_id)
                        .then(ress => {

                            if (ress.status_code !== '404') {
                                let dataOrderPayment = {};
                                Object.keys(ress).forEach(rb => {
                                    if (
                                        rb === "fraud_status" ||
                                        rb === "payment_type" ||
                                        rb === "status_code" ||
                                        rb === "transaction_id" ||
                                        rb === "transaction_status" ||
                                        rb === "transaction_time" ||
                                        rb === "pdf_url" ||
                                        rb === "order_id" ||
                                        rb === "masked_card" ||
                                        rb === "bank" ||
                                        rb === "card_type" ||
                                        rb === "payment_code"
                                    ) {

                                        dataOrderPayment[rb] = ress[rb];
                                    }
                                    if (rb === "payment_type" && ress[rb] === "cstore") {
                                        dataOrderPayment["store"] = "indomaret";
                                    }
                                    if (rb === "payment_type" && ress[rb] === "mandiri_clickpay") {
                                        dataOrderPayment["bank"] = "mandiri";
                                    }
                                    if (rb === "payment_type" && ress[rb] === "danamon_online") {
                                        dataOrderPayment["bank"] = "danamon";
                                    }
                                    if (rb === "payment_type" && ress[rb] === "cimb_clicks") {
                                        dataOrderPayment["bank"] = "cimb";
                                    }
                                    if (rb === "payment_type" && ress[rb] === "bri_epay") {
                                        dataOrderPayment["bank"] = "bri";
                                    }
                                    if (rb === "permata_va_number") {
                                        dataOrderPayment["bank"] = "permata";
                                        dataOrderPayment["va_number"] = ress[rb];
                                    }
                                    if (rb === "va_numbers") {
                                        dataOrderPayment["bank"] = ress[rb][0].bank;
                                        dataOrderPayment["va_number"] = ress[rb][0].va_number;
                                    }
                                    if (rb === "gross_amount") {
                                        dataOrderPayment[rb] = parseInt(ress[rb]);
                                    }

                                });
                                if (Object.keys(dataOrderPayment.length > 0)) {

                                    db.query(queryOrderPaymentInsert, [dataOrderPayment], (err, result) => {
                                        if (err) {
                                            callback(err, null);
                                        }
                                        if (result) {
                                            if (ress.status_code.match(/^[4]/g) || ress.status_code.match(/^[5]/g)) {
                                                db.query(queryUpdateOnlyStatus, ['202', dataOrderPayment.order_id], (err, result) => {
                                                    callback(err, 'ok');
                                                })
                                            } else {
                                                callback(null, 'ok');
                                            }

                                        }
                                    });

                                } else {
                                    callback(null, null);
                                }
                            } else {
                                callback(null, null);
                            }
                        }).catch(error => {

                            
                            if (error.ApiResponse) {
                                if (error.ApiResponse.status_code === '404') {
                                    callback(null, null);
                                } else {
                                    callback(error.ApiResponse, null);
                                }

                            }
                        })
                }

                if (result.length > 0) {
                    // UPDATE PAYMENT
                    snap.transaction.status(order_id).then(ress => {

                        //  UPDATE STATUS AND ORDER PAYMENT IF NOT CANCEL
                        if (ress.status_code !== result[0].status_code
                            && ress.transaction_status !== "cancel"
                            && (!ress.status_code.match(/^[4]/g) || !ress.status_code.match(/^[5]/g))
                            && (ress.status_code.match(/^[2]/g) && ress.status_code !== '202')
                            && result[0].order_status_code !== "ok") {
                            db.query(queryUpdateStatusOrder, [
                                ress.status_code,
                                ress.status_code,
                                ress.transaction_status,
                                ress.transaction_id], (err, result) => {
                                    callback(err, 'ok');
                                })
                        }

                        // CANCELED BY MIDTRANS
                        if (ress.status_code !== result[0].status_code
                            && result[0].order_status_code !== "ok"
                            && (ress.transaction_status == "cancel" || ress.status_code === '202' || ress.status_code.match(/^[4]/g) || ress.status_code.match(/^[5]/g))
                        ) {
                            db.query(queryUpdateStatusOrder, [
                                "202",
                                ress.status_code,
                                ress.transaction_status,
                                ress.transaction_id], (err, result) => {
                                    callback(err, 'ok');
                                })
                        }


                        //EDIT BY ADMIN AND UPDATE STATUS PAYMENT AND ORDER
                        if (ress.status_code !== result[0].status_code
                            && (!ress.status_code.match(/^[4]/g) || !ress.status_code.match(/^[5]/g))
                            && (ress.status_code.match(/^[2]/g) && ress.status_code !== '202' && ress.transaction_status !== "cancel")
                            && result[0].order_status_code === "ok") {
                            db.query(queryUpdatePaymentOrder, [
                                ress.status_code,
                                ress.status_code,
                                ress.transaction_status,
                                ress.transaction_id], (err, result) => {
                                    callback(err, 'ok');
                                })
                        }
                        // EDIT BY ADMIN AND UPDATE STATUS PAYMENT AND ORDER = CANCEL 
                        if (ress.status_code !== result[0].status_code
                            && result[0].order_status_code === "ok"
                            && (ress.transaction_status === "cancel" || ress.status_code === '202' || ress.status_code.match(/^[4]/g) || ress.status_code.match(/^[5]/g))
                        ) {
                            db.query(queryUpdatePaymentOrder, [
                                "202",
                                ress.status_code,
                                ress.transaction_status,
                                ress.transaction_id], (err, result) => {
                                    callback(err, 'ok');
                                })
                        }
                        if (ress.status_code == result[0].status_code) {
                            callback(null, null);
                        }


                    }).catch(error => {
                        
                        if (error.ApiResponse) {
                            if (error.ApiResponse.status_code === '404') {
                                callback(null, null);
                            } else {
                                callback(error.ApiResponse, null);
                            }

                        }

                    });
                }

            })

        },
        orders: function (callback) {
            db.query(queryOrder, (err, result) => {
                callback(err, result);
            })
        },
        order_item: function (callback) {
            db.query(queryOrderItems, (err, result) => {
                callback(err, result);
            })
        },
        order_shipment: function (callback) {
            db.query(queryOrderShipment, (err, result) => {
                callback(err, result);
            })
        },
        order_voucher: function (callback) {
            db.query(queryOrderVoucher, (err, result) => {
                callback(err, result);
            })
        },
        order_billing: function (callback) {
            db.query(queryOrderBill, (err, result) => {
                callback(err, result);
            })
        },
        order_payment: function (callback) {
            db.query(queryOrderPayment, (err, result) => {
                callback(err, result);
            })
        }

    }, function (err, result) {
        if (err) {
            let notification = {
                error: true,
                message: "There is an error !",
                notification: true
            }
            return res.status(400).json(err);
        }
        if (result) {
            return res.status(200).json({ data: result });
        }
    })
}


export const deleteSelectedOrder = (req,res)=>{
    if (!req.body instanceof Array && req.body.length === 0) {
        return res.status(400).json({ errors: 'MUST BE PROVIDED' });
    }
    let queryDeleteProduct = `DELETE from orders where id in ?`;
    db.query(queryDeleteProduct, [[req.body]], (err, result) => {
        if (err) return res.status(400).json(err);
        if (result) {
            let notification = {
                error: false,
                message: "ORDER HAS BEEN DELETED",
                notification: true
            }
            return res.status(200).json({ data: result, notification: notification });
        }
    })
}

export const updateSelectedOrder = (req,res)=>{
    if (!req.body.id instanceof Array && req.body.id.length === 0 || req.body.order_status_id === "" ||typeof req.body.order_status_id === "undefined") {
        return res.status(400).json({ errors: 'MUST BE PROVIDED' });
    }

    let queryFindOrder = `SELECT * FROM orders where id in ?`;
    db.query(queryFindOrder,[[req.body.id]],(err,result)=>{
        if(err){
            return res.status(400).json(err);
        }
        if(result.length > 0){
            let queryStatusId = [];
            result.forEach(r=>{
                queryStatusId.push(`when order_status_id = ${r.order_status_id} then ${req.body.order_status_id}`);
            });
            const queryUpdateOrder = `UPDATE orders set order_status_id = (case ${queryStatusId.toString().replace(/[,]/g, ' ')} end) where id in ? `;
            db.query(queryUpdateOrder,[[req.body.id]],(err,result)=>{
                if (err) {
                    return res.status(400).json(err);
                };
                if(result){
                    let notification = {
                        error: false,
                        message: "ORDER STATUS HAS BEEN UPDATED",
                        notification: true
                    }
                    return res.status(200).json({  notification: notification });
                }
            })
        }
        if(result.length === 0){
            return res.status(400).json({errors:'NOT FIND ANY ID'});
        }
    })
}


function getDataOrderFull(){
    let querySelectAll = `
    SELECT
    ord.id as order_id,
    ord.user_id,
    ord.order_status_id,
    ord.created_at,
    orb.email,
    orb.phone_number,
    orp.payment_type,
    orp.gross_amount,
    orp.status_code,
    orp.bank,
    orp.va_number,
    orp.payment_code,
    orp.store,
    orp.pdf_url,
    orp.card_type,
    orp.masked_card,
    orp.transaction_id,
    orp.transaction_status,
    orp.transaction_time,
    orp.transaction_time + interval 1 day as expired_date,
    ors.courier,
    ors.description,
    ors.service,
    ors.cost,
    ors.etd,
    os.status as order_status
    from orders as ord
    left join order_billing as orb on ord.id = orb.order_id
    left join order_payment as orp on ord.id = orp.order_id
    left join order_shipment as ors on ord.id = ors.order_id
    left join order_status as os on ord.order_status_id = os.id
    order by ord.updated_at desc `;
   return new Promise((res,rej)=>{
       db.query(querySelectAll,(err,result)=>{
           if (result) return res(result);
           if(err) return rej(rej);
       })
    })
}

export const downloadDataFull =async (req,res)=>{
   let data = await getDataOrderFull();
    const header = Object.keys(data[1]);
    let dataCsv = [
        // ['NIS','Nama','Tanggal Lahir'],[murid.nis,murid.nama,murid.tanggalLahir],
        // ['Kelas','Semester'],[kelas,semester],
        header];

    for (const d of data) {
        dataCsv.push(
            [   d.order_id,
                d.user_id,
                d.order_status_id,
                d.created_at,
                d.email,
                d.phone_number,
                d.payment_type,
                d.gross_amount,
                d.status_code,
                d.bank,
                d.va_number,
                d.payment_code,
                d.store,
                d.pdf_url,
                d.card_type,
                d.masked_card,
                d.transaction_id,
                d.transaction_status,
                d.transaction_time,
                d.expired_date,
                d.courier,
                d.description,
                d.service,
                d.cost,
                d.etd,
                d.order_status]
        );
    }
    const ws = XLSX.utils.aoa_to_sheet(dataCsv);
    const wb = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(wb, ws, "dataOrderFull");

    var fileName = `dataOrderFull.csv`;
    res.setHeader('Content-disposition', 'attachment; filename=' + fileName);
    res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    var buf = XLSX.write(wb, { type: 'buffer', bookType: "csv" });
    res.set('Content-Type', 'text/csv');

    res.send(buf);
}


export const getOrder = (req,res)=>{
    let offset = 0;
    let perPage = 4;
    let category;
    if(req.params.category == "pending"){
        category =`where os.id = 3`;
    }
    if(req.params.category == "onprogress"){
        category = `where os.id = 4`;
    }
    if (req.params.category == "cancelled"){
        category = `where os.id = 2 or os.id =6`;
    }
    if (req.params.category == "completed") {
        category = `where os.id = 5`;
    }
    if (req.params.category == "today") {
        category = `where DATE(ord.created_at) = CURDATE() or DATE(ord.updated_at) = CURDATE()  `;
    }
        if (typeof req.query.page !== "undefined" && req.query.page > 0) {
        offset = (parseInt(req.query.page) - 1) * perPage;

    }
    let search = req.query;
    
    if (typeof search.search !== "undefined" && search.search !== '' ){
        category = `where 
        ord.id like '%${search.search}%' or
        orb.email like '%${search.search}%' or 
        orb.phone_number like '%${search.search}%' or 
        ors.courier like '%${search.search}%' or 
        orp.status_code like '%${search.search}%' or
        os.status like '%${search.search}%'
        `;
    }
    const querySelectOrderProgress = `SELECT 
    ord.id as order_id,
    ord.user_id,
    ord.order_status_id,
    ord.created_at,
    orb.email,
    orb.phone_number,
    orp.payment_type,
    orp.gross_amount,
    orp.status_code,
    orp.transaction_id,
    orp.transaction_status,
    orp.transaction_time,
    ors.courier,
    ors.description,
    ors.service,
    ors.cost,
    ors.etd,
    os.status as order_status
    from orders as ord
    left join order_billing as orb on ord.id = orb.order_id
    left join order_payment as orp on ord.id = orp.order_id
    left join order_shipment as ors on ord.id = ors.order_id
    left join order_status as os on ord.order_status_id = os.id
    ${category ? category : '  order by ord.updated_at desc '}
  
    limit ${perPage} offset ${offset}  `;

    const queryCountPagination = ` 
    SELECT count(*) as totalPage
    from orders as ord
    left join order_billing as orb on ord.id = orb.order_id
    left join order_payment as orp on ord.id = orp.order_id
    left join order_shipment as ors on ord.id = ors.order_id
    left join order_status as os on ord.order_status_id = os.id
    ${category ? category : ' order by ord.updated_at desc '}
   `;

    async.parallel({
        orders: function (callback) {
            db.query(querySelectOrderProgress, (err, result) => {
                callback(err,result);
            })
        },
        pagination: function (callback) {

            db.query(queryCountPagination, (err, result) => {

                if (err) {
                    callback(err, null);
                }
                if (result.length > 0) {
                    let total_page = Math.round(result[0].totalPage / perPage);
                    let current_page = result[0].totalPage / perPage;
                    let data = {
                        total_page: total_page,
                        current_page: (offset / perPage) + 1,
                        perPage: perPage,
                        results: result[0].totalPage
                    }
                    callback(err, data);
                }
                if (result.length === 0) {
                    let data = {
                        total_page: 0,
                        current_page: offset + 1,
                        perPage: perPage,
                        results: 0
                    }
                    callback(err, data);
                }

            });
        }
    }, function (err, result) {
        if (err) return res.status(400).json(err);
        if (result) {
            return res.status(200).json(result);
        }
    })
  










//     let querySearch = '';
//     let search;
//     let offset = 0;
//     let perPage = 4;
//     if (req.query.search) {
//         search = req.query.search;
//         querySearch = `
//         where u.id like '%${search}%' or  u.displayName like '%${search}%' or u.lastname like '%${search}%'
//         or u.email like  '%${search}%' or ui.phone_number like '%${search}%' or p.name like '%${search}%'
//         or r.name like '%${search}%'   or d.name like '%${search}%' or v.name like '%${search}%'
//         `
//     }

//     if (typeof req.query.page !== "undefined" && req.query.page > 0) {
//         offset = (parseInt(req.query.page) - 1) * perPage;

//     }


//     let querySelectAll = `
// SELECT 
// 	u.id,u.displayName,u.firstname,u.lastname,u.email,u.gender,u.is_provider, 
// 	p.name as province,r.name as regency,d.name as district,v.name as village,ud.address,ud.postcode,
// 	ui.birthday,ui.location,ui.age,ui.phone_number
// from user as u
// left join user_address as ud on u.id = ud.user_id
// left join provinces as p on ud.province_id = p.id
// left join regencies as r on ud.regency_id = r.id
// left join districts as d on ud.district_id = d.id
// left join villages as v on ud.village_id = v.id
// left join user_information as ui on u.id = ui.user_id
// left join user_provider as up on u.id = up.user_id
// ${querySearch}
// order by u.created_at desc
// limit ${perPage} offset ${offset}  
// `;

//     let queryCountPagination = `
// SELECT count(*) as totalPage
// from user as u
// left join user_address as ud on u.id = ud.user_id
// left join provinces as p on ud.province_id = p.id
// left join regencies as r on ud.regency_id = r.id
// left join districts as d on ud.district_id = d.id
// left join villages as v on ud.village_id = v.id
// left join user_information as ui on u.id = ui.user_id
// left join user_provider as up on u.id = up.user_id
// ${querySearch}
// order by u.created_at desc
// `;

    // async.parallel({
    //     users: function (callback) {
    //         db.query(querySelectAll, (err, result) => {
    //             callback(err, result)
    //         })
    //     },
    //     pagination: function (callback) {

    //         db.query(queryCountPagination, (err, result) => {

    //             if (err) {
    //                 callback(err, null);
    //             }
    //             if (result.length > 0) {
    //                 let total_page = Math.round(result[0].totalPage / perPage);
    //                 let current_page = result[0].totalPage / perPage;
    //                 let data = {
    //                     total_page: total_page,
    //                     current_page: (offset / perPage) + 1,
    //                     perPage: perPage,
    //                     results: result[0].totalPage
    //                 }
    //                 callback(err, data);
    //             }
    //             if (result.length === 0) {
    //                 let data = {
    //                     total_page: 0,
    //                     current_page: offset + 1,
    //                     perPage: perPage,
    //                     results: 0
    //                 }
    //                 callback(err, data);
    //             }

    //         });
    //     }
    // }, function (err, result) {
    //     if (err) return res.status(400).json(err);
    //     if (result) {
    //         return res.status(200).json(result);
    //     }
    // })


}