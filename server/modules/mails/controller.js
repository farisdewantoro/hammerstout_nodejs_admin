import db from '../../config/conn';
import async from 'async';
import nodemailer  from 'nodemailer';
import fs from 'fs'
import path from 'path';
import {renderToString}  from 'react-dom/server';
import Email from '../../../Email';
import moment from 'moment';
let queryOrderDetail = `
select 
	ord.id,
	orp.payment_type,orp.bank,orp.store,orp.store,
	orp.va_number,orp.payment_code,orp.gross_amount,orp.pdf_url,orp.transaction_time,
	orp.transaction_time + interval 1 day as expired_date
 from orders as ord
 left join order_payment as orp on ord.id = orp.order_id 
 where ord.id = ?
 `;

 let queryOrderBilling = `
 select 
	ord.id,
	orb.firstname,
	orb.email,
	orb.phone_number,
	orb.province,
	orb.regency,
	orb.district,
	orb.village,
	orb.postcode,
	orb.address
from orders as ord
left join order_billing as orb on ord.id = orb.order_id
 where ord.id = ?
 `;

 let queryOrderItem = `
SELECT 
    p.name as product_name,
    oi.price,
    p.regular_price,
    oi.order_id,
    p.id as product_id,
    pa.size,
    oi.quantity 
    from order_item as oi 
    left join orders as ord on oi.order_id = ord.id
    left join products as p on oi.product_id = p.id
    left join product_attribute as pa on oi.product_attribute_id = pa.id
    where ord.id = ?
 `;

 function getOrderDetail(id){
   return  new Promise((res,rej)=>{
         db.query(queryOrderDetail,[id],(err,result)=>{
             if(err) return rej(err);
             if(result.length > 0) return res(result[0]);
             if(result.length === 0) return rej('NOT FIND');
         })
     })
 }

 function getOrderBilling(id){
    return new Promise((res,rej)=>{
        db.query(queryOrderBilling,[id],(err,result)=>{
            if (err) return rej(err);
            if (result.length > 0) return res(result[0]);
            if (result.length === 0) return rej('NOT FIND');
        })
    })
 }

 function getOrderItem(id){
     return new Promise((res, rej) => {
         db.query(queryOrderItem, [id], (err, result) => {
             if (err) return rej(err);
             if (result.length > 0) return res(result);
             if (result.length === 0) return rej('NOT FIND');
         })
     })
 }

export const previewMail = async (req,res)=>{
    let users = [
        {
            email: 'devfarisdewantoro@gmail.com',
            subject:'MANTAP',
        },

    ];
    let order_billing = await getOrderBilling('78OE9A3');
    
    let order_detail = await getOrderDetail('78OE9A3');
    let order_item = await getOrderItem('78OE9A3');
    if (order_billing && order_detail && order_item){
        order_detail.transaction_time = moment(order_detail.transaction_time).format('LLL');
        order_detail.expired_date = moment(order_detail.expired_date).format('LLL');
        users[0].order_detail = order_detail;
        users[0].order_billing = order_billing;
        users[0].order_item = order_item;

    }
   
    let info = await Email.loadTemplate('welcome', users);
    return res.status(200).send(info[0].email.html);
}


export const sendMail = async (req,res)=>{
    let users = [
        {
            name: 'Joe',
            email: 'devfarisdewantoro@gmail.com',
            subject: 'MANTAP'
        },
     
    ];
    Email.loadTemplate('welcome', users).then(result=>{
    if(result){
    let info = result.map((res2) => {
       return Email.sendEmail({
            to: res2.context.email,
            from: '"Hammer" <farisdewantoro@hammerstoutdenim.com>',
            subject: res2.email.subject,
            html: res2.email.html
        });
    });
    return Promise.all(info).then((result)=>{
        return res.status(200).json('ok');
    }).catch((err)=>{
        console.log(err);
    })

    }
});
    

}
